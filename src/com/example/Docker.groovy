package com.example

class Docker implements Serializable {
    def script

    Docker(script) {
        this.script = script
    }

    def dockerBuild(String imageName) {
        script.echo "building the docker image..."
        script.sh "docker build -t $imageName ."
    }

    def dockerLogin() {
        script.withCredentials([script.usernamePassword(credentialsId: 'dockerhub-credentials', passwordVariable: 'PASS', usernameVariable: 'USER')]) {
            script.sh 'echo $PASS | docker login -u $USER --password-stdin'
        }
    }

    def dockerPush(String imageName) {
        script.sh "docker push $imageName"
    }
}